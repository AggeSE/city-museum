const themeDir = "./";
let mix = require("laravel-mix");
require("mix-tailwindcss");

mix
  .setPublicPath("./dist/")
  .setResourceRoot("/web/themes/custom/city_museum/dist/")
  .webpackConfig({
    output: {
      publicPath: "/web/themes/custom/city_museum/dist/",
    },
  })
  .css(`${themeDir}/src/css/main.css`, `${themeDir}/dist`)
  .tailwind()
  .minify(`${themeDir}/dist/main.css`)
  .options({
    processCssUrls: false,
  })
  .sourceMaps();
