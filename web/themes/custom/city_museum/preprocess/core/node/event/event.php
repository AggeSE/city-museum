<?php

use Drupal\node\Entity\Node;
/**
 * Implements template_preprocess_node__event().
 */
function city_museum_preprocess_node__event(&$variables) {

  $nid = $variables['node']->id();

  $node = Node::load($nid);

  // Get the number of tickets value
  $tickets_amount = $node->get('field_number_of_tickets')->value;

  // Frontend utility classes
  $tickets_status_classes = 'p-1 rounded-md inline-block font-bold';

  // Check if the number of tickets is greater or equal 0, if not, display 'Sold out'
  if ($tickets_amount !== null && $tickets_amount <= 0) {
      $variables['tickets_status'] = [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#value' => 'SOLD OUT',
        '#attributes' => [
        'class' => [$tickets_status_classes, 'bg-red-500', 'text-white'],
        ],
    ];
  }
  // Check if the number of tickets is less than or equal to 10, if so, display 'Only x seats left'
  else if ($tickets_amount !== null && $tickets_amount <= 10) {
    $variables['tickets_status'] = [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#value' => $tickets_amount . ' ' . 'seat left',
      '#attributes' => [
      'class' => [$tickets_status_classes, 'bg-yellow-400', 'text-black'],
      ],
  ];
  }
}