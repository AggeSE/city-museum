/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./templates/**/*.{html,js,twig}", "./preprocess/**/*.php"],
  theme: {
    extend: {},
  },
};
